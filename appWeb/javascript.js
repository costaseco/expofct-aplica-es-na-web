// Anima o botão, fazendo-o piscar
function animarBotao() {
  $("#saltar").animate({"opacity": "toggle"}, 1000, animarBotao);
}

/*
Esta função devolve um número inteiro aleatório entre minimo e maximo

Para os curiosos:
- Math.floor(x) devolve o maior inteiro menor ou igual do que x
  (e.g. Math.floor(5.753) = 5)
- Math.random() devolve um valor aleatório no intervalo [0, 1[
*/
function numeroAleatorio(minimo, maximo) {
  return Math.floor(Math.random() * (maximo - minimo + 1)) + minimo;
}

$(document).ready(function() {

});
