<?
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type');


//<form method=GET action=urlimages.php>
//Web Site: <input type="text" name="url"> <input type="submit" value="Get Image!">
//</form>
//<?
$url = $_REQUEST['url'];
$url = checkValues($url);
$urlhash = md5(strtolower($url));
if (file_exists("cache/".$urlhash)){
	//if (false && filemtime("cache/".md5($url))+1800<time()){
	//	unlink("cache/".md5($url));
	//} else {
		echo file_get_contents("cache/".$urlhash);
		die();
	//}
}

//echo "URL: ".$url."<br>";

function checkValues($value){
	$value = trim($value);
	if (get_magic_quotes_gpc()){
		$value = stripslashes($value);
	}
	$value = strtr($value, array_flip(get_html_translation_table(HTML_ENTITIES)));
	$value = strip_tags($value);
	$value = htmlspecialchars($value);
	return $value;
}

function fetch_record($path){
	$file = fopen($path, "r"); 
	if (!$file){
		exit("Problem occured");
	}
	$data = '';
	while (!feof($file)){
		$data .= fgets($file, 1024);
	}
	return $data;
}

function download_http($url){
      $parts = parse_url($url);

        $fp = fsockopen($parts["host"], 80, $errno, $errstr, 30);
        if (!$fp) {
            echo "$errstr ($errno)<br />\n";
        } else {
            $out = "GET ".$parts["path"]."?".$parts["query"]." HTTP/1.1\r\n";
            $out .= "Host: ".$parts["host"]."\r\n";
            $out .= "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.79 Safari/535.11";
            $out .= "Connection: Close\r\n\r\n";
            fwrite($fp, $out);
            $data = "";
            while (!feof($fp)) {
                $data .= fgets($fp, 1024);
            }
            fclose($fp);
        }
        return $data;

//        return file_get_contents($url);
}

//$pageSource = fetch_record($url);
$pageSource = file_get_contents($url);
//$pageSource = download_http($url);

$titleTagRegex = "/<title>(.+)<\/title>/i";
preg_match_all($titleTagRegex, $pageSource, $titleTags, PREG_PATTERN_ORDER);
$titleTag = $titleTags[1][0];

// Meta Tags
$metaTags = get_meta_tags($url);
if (array_key_exists('title', $metaTags)){
	$title = $metaTags['title'];
} else {
	$title = $titleTag;
}

$desc = $metaTags['description'];

//og:image
/*echo $pageSource;
$ogImageStart = strpos($pageSource, "og:image");
echo $ogImageStart."a";
if ($ogImageStart!==FALSE){
echo "b";
        $ogImageStart = strpos($pageSource, "content=\"",$ogImageStart)+9;
        $ogImage = substr($pageSource, $ogImageStart, strpos($pageSource, "\"",$ogImageStart)-$ogImageStart);
echo $ogImage;
}*/

// fetch images
$document = new DOMDocument();
libxml_use_internal_errors(true);
$document->loadHTML($pageSource);
libxml_clear_errors();

$images_array = array();

$tags = $document->getElementsByTagName('img');
foreach($tags as $img){
        $url = $img->getAttribute('src');
        if(!$url)
                continue;

        $images_array[] = $url;
}

$metaTags = $document->getElementsByTagName('meta');
foreach ($metaTags as $meta){
	switch($meta->getAttribute('property')){
		case "og:image":
			$images_array[] = $meta->getAttribute('content');
			break;
	}
}

$linkTags = $document->getElementsByTagName('link');
foreach($linkTags as $link){
	if ($link->getAttribute('rel')=='image_src'){
		$images_array[] = $link->getAttribute('href');
	}
}

$image_regex = '/<img[^>]*'.'src=[\"|\'](.*)[\"|\']/Ui';
preg_match_all($image_regex, $pageSource, $img, PREG_PATTERN_ORDER);
$imagesREGEX_array = $img[1];

foreach($imagesREGEX_array as $regeximg){
	if (!in_array($regeximg, $images_array)){
		$images_array[] = $regeximg;
	}
}

//include('simplehtmldom/simple_html_dom.php');
//$html = str_get_dom($pageSource);
// Find all images 
//foreach($html->find('img') as $element) 
//	       echo $element->src . '<br>';

$ignoreLinks = split("\n",file_get_contents("ignoreLinksHash.txt"));
//echo "Ignore cache size: ".sizeof($ignoreLinks)."<br>";
$images = array();
//echo "Total images found: ".sizeof($images_array)."<br>";
//echo "Images found (after filtering):<br>\n";
$ignoreWords = array("doubleclick.net", "pub.sapo.pt");


for ($i=0;$i<=sizeof($images_array) && sizeof($images)<50;$i++){
	$imgLink = $images_array[$i];
	if (strpos($imgLink,"http")===FALSE){
		$parts = parse_url($url);
		if (substr($imgLink,0,1)=="/"){
			$imgLink = $parts['scheme']."://".$parts['host'].$imgLink;
		} else {
 			$imgLink = $parts['shceme']."://".$parts['host'].$parts['path'].$imgLink;
		}
	}

	$skipImage = false;
	for($j=0;!$skipImage && $j<sizeof($ignoreWords);$j++){
		$skipImage = strpos($imgLink,$ignoreWords[$j]);
	}

	if(!in_array(md5($imgLink), $ignoreLinks) && !$skipImage){
		$imgSize = @getimagesize($imgLink);
		if($imgSize){
			list($width, $height, $type, $attr) = $imgSize;
			$ratio = $width/$height;
			if($width >= 100 && $height >= 100 && $ratio>1 && $ratio<3){
				//echo "[".sizeof($images)."] = ".$imgLink." ".$width."x".$height." @ ".number_format($ratio,3)."<br>";
				$images[] = array(
						"url"=>$imgLink,
						"width"=>$width,
						"height"=>$height
					);
			} else {
				$ignoreLinks[] = md5($imgLink);
			}
		}
	}
}

file_put_contents("ignoreLinksHash.txt",implode("\n",$ignoreLinks));

$maxH = 0;
$maxW = 0;
$bestRatio = 0;
$imageIndex = -1;
for($i=0;$i<sizeof($images);$i++){
	if ($images[$i]["width"]>$maxW && $images[$i]["height"]>$maxH){
		$imageIndex = $i;
	} 
	$maxW = max($maxW, $images[$i]["width"]);
	$maxH = max($maxH, $images[$i]["height"]);
	$bestRatio = min($bestRatio, $images[$i]["width"]/$images[$i]["width"]);
}

//echo "MaxH: ".$maxH."<br>";
//echo "MaxW: ".$maxW."<br>";
//echo "Best Ratio: ".$bestRatio."<br>";
//echo "Index: ".$imageIndex."<br>";

//echo "URL: ".$url."<br>";
//echo "Title: ".$title."<br>";
//echo "Description: ".$desc."<br>";

//echo "<br>";
//echo "Selected image is:<br>";
//echo "<img src=\"".$images[$imageIndex]["url"]."\">";


$json = json_encode($images[$imageIndex]);
echo $json;
if ($json!='null')
	file_put_contents("cache/".$urlhash, $json);
?>
