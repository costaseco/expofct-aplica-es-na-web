var feeds = new Array();
var sim = true;
var nao = false;

var verde = 'green';

// Esta função é executada ao adicionar um novo RSS
function adicionarRSS(url){
	obterRSS(url, corFundo, function(tituloFeed, descricaoFeed, itemsFeed, corFundo){
			feeds.push({
				titulo: tituloFeed,
				descricao: descricaoFeed,
				items: itemsFeed,
			});
			hideAddRSSForm();
			criaBloco(feeds.length-1,corFundo);
		}
	);

}

function toggleAddRSSForm(){  
	if(!$('#addRSSForm').is(':visible')){  
		$("#backgroundPopup").fadeIn('normal');  
		$("#addRSSForm").fadeIn('normal');
		$('#rssurl').focus();
	} else {
		$("#backgroundPopup").fadeOut('normal');  
		$("#addRSSForm").fadeOut('normal');  
	}
}

function hideAddRSSForm(){
		$("#backgroundPopup").fadeOut('normal');  
		$("#addRSSForm").fadeOut('normal');  
}

function centerAddRSSForm(){
	var windowWidth = document.documentElement.clientWidth;  
	var windowHeight = document.documentElement.clientHeight;  
	var popupHeight = $("#addRSSForm").height();  
	var popupWidth = $("#addRSSForm").width();  

	$("#addRSSForm").css({  
		"position": "absolute",  
		"top": windowHeight/2-popupHeight/2,  
		"left": windowWidth/2-popupWidth/2  
	});  
  
	$("#backgroundPopup").css({  
		"height": windowHeight  
	}); 
}  

function obterRSS(url, corFundo, callback){
	console.log('obterRSS: '+url);
	var fun = callback;
	$.jGFeed(url, 
		function(feeds){
			if(!feeds){
				alert('there was an error');
			}
			var items = new Array();
			for(var i=0;i<feeds.entries.length;i++){
				var entry = feeds.entries[i];
				items[i] = {
					"titulo": entry.title,
					"url": entry.link,
					"descricao": entry.contentSnippet,
					"data": entry.publishedDate,
					"autor": entry.author,
					"imagem": null
				};
			}
			console.log(items.length+' items de '+url);
			
			fun && fun(feeds.title, feeds.description, items, corFundo);
		}, 20);	
}


/*
obterImageLink(items[idx].url, function(imageURL){
	alert(imageURL);
	imagesCount++;
	items[idx].image = imageURL;
});

*/
function obterImageLink(url, callback){
	console.log('obterImageLink: '+url);
	$.ajax({
	    type: 'POST',
	    url: 'http://miguel.domingues.pt/expofct/urlimages.php',
	    crossDomain: true,
	    data: 'url='+encodeURI(url),
	    success: function(responseData, textStatus, jqXHR) {
	        console.log(url+' images: '+responseData);
	        var json = jQuery.parseJSON(responseData);
	        callback(json);
	    },
	    error: function (responseData, textStatus, errorThrown) {
	        //TODO: alert('POST failed.');
	    }
	});
}



function criaBloco(feedIndex, corFundo){
	var bloco = criarBlocoHTML(feedIndex, corFundo);
	var feed = feeds[feedIndex];
	obterImagemSeguinte(feedIndex, 0);
}

function obterImagemSeguinte(feedIndex, imagemIndex){
	console.log('obterImagem: feed['+feedIndex+'], image: '+imagemIndex);
	if (imagemIndex < feeds[feedIndex].items.length){
		if (feeds[feedIndex].items[imagemIndex].imagem){
			obterImagemSeguinte(feedIndex, imagemIndex+1);
		} else {
			obterImageLink(feeds[feedIndex].items[imagemIndex].url, function(imageData){
				if (imageData){
					feeds[feedIndex].items[imagemIndex].imagem = imageData;
					
					if (imagemIndex==0){
						var bloco_imagem = $('#bloco'+feedIndex).find('.bloco_imagem:first');
						bloco_imagem.hide();
						bloco_imagem.css('background-image', 'url('+feeds[feedIndex].items[imagemIndex].imagem.url+')');
						bloco_imagem.css('background-size', 'contain');
						bloco_imagem.html('');
						bloco_imagem.fadeIn('normal');
					}
				} else if (imagemIndex==0){
					var bloco_imagem = $('#bloco'+feedIndex).find('.bloco_imagem:first');
					bloco_imagem.html('');
				}
				obterImagemSeguinte(feedIndex, imagemIndex+1);
			});
		}
	}
}

function criarBlocoHTML(feedsIndex, corFundo){
	var feed = feeds[feedsIndex];
	var bloco = $('<div class="bloco" id="bloco'+feedsIndex+'"></div>');
	
	var close = $('<div class="close"></div>');
	bloco.append(close);
	close.click(function(){
		console.log('close click');
		close.remove();
		bloco.remove();
		return false;
	});

	var bloco_fundo = $('<div class="bloco_fundo"></div>');
	bloco_fundo.css('background-color', corFundo);
	bloco.append(bloco_fundo);
	
	var bloco_imagem = $('<div class="bloco_imagem"></div>');
	if (feed.items[0].imagem){
		bloco_imagem.css('background-image', 'url('+feed.items[0].imagem.url+')');
		bloco_imagem.css('background-size', 'contain');
	} else {
		//bloco_imagem.css('background-image', 'url(images/loader-big.gif)');
		bloco_imagem.css('background-size', 'auto');
		bloco_imagem.html('<br><br><br><br><br>&nbsp; &nbsp;Carregando…');
	}
	bloco.append(bloco_imagem);

	var bloco_titulo = $('<div class="bloco_titulo"></div>');
	bloco_titulo.append(feed.titulo);
	bloco.append(bloco_titulo);
	
	var bloco_descricao = $('<div class="bloco_descricao"></div>');
	var span_descicao_texto = $('<div class="bloco_descricao_texto"></div>');
	span_descicao_texto.append(feed.descricao);
	bloco_descricao.append(span_descicao_texto);
	bloco.append(bloco_descricao);
	
	bloco.hide();
	
	$('#blocos').append(bloco);
	bloco.fadeIn('normal');
	
	bloco.click(function(){
		console.log('bloco click');
		verFeed(feedsIndex,0);
	});
	return bloco;
}

function verFeed(feedsIndex,page){
	console.log('verFeed: '+feedsIndex+' page:'+page);
	//TODO: check if feed has enough items
	var id = 'feed'+feedsIndex+"_page"+page;
	if ($('#'+id).length>0){
		//$('#'+id).remove();
		return;
	}
	
	var feed = feeds[feedsIndex];
	var section = $('<section id="'+id+'" class="feed"></section>');
	
	var current = page*4;
	var margin = 5;
	var padding = 15;
	var topBar = 35;
	var border = 1;
	
	section.css('width', $(document).width()+'px');
	section.css('height', ($(document).height()-topBar)+'px');
	
	var feedA = $('<div class="feedA"></div>');
	feedA.append(feed.items[current].titulo);
	var widthA = $(document).width()*0.5-margin*2-padding*2-border*2;
	feedA.css('width', widthA+'px');
	feedA.css('height', ($(document).height()-margin*2-padding*2-border*2-topBar)+'px');
	feedA.css('margin', margin+'px');
	feedA.css('padding', padding+'px');
	if (feed.items[current].imagem){
		var img = $('<img src="'+feed.items[current].imagem.url+'" />');
		feedA.append(img);
	}
	var descA = $('<span>'+feed.items[current].descricao+'</span>');
	feedA.append(descA);
	var urlA = feed.items[current].url;
	feedA.click(function(){
		abirIFrame(urlA);
	});
	
	current++;
	var feedB = $('<div class="feedB"></div>');
	feedB.append(feed.items[current].titulo);
	feedB.css('left', ($(document).width()*0.5)+'px');
	var widthB = $(document).width()*0.5-margin*2-padding*2-border*2;
	feedB.css('width', widthB+'px');
	var heightB = ($(document).height()-topBar)*0.5-margin*2-padding*2-border*2;
	feedB.css('height', heightB+'px');
	feedB.css('margin', margin+'px');
	feedB.css('padding', padding+'px');
	if (feed.items[current].imagem){
		var img = $('<img src="'+feed.items[current].imagem.url+'" />');
		feedB.append(img);
	}
	var descB = $('<span>'+feed.items[current].descricao+'</span>');
	feedB.append(descB);
	var urlB = feed.items[current].url;
	feedB.click(function(){
		abirIFrame(urlB);
	});	

	current++;
	var feedC = $('<div class="feedC"></div>');
	feedC.append(feed.items[current].titulo);
	feedC.css('top', (($(document).height()-topBar)*0.5)+'px');
	feedC.css('left', ($(document).width()*0.5)+'px');
	var widthC = $(document).width()*0.25-margin*2-padding*2-border*2;
	feedC.css('width', widthC+'px');
	feedC.css('height', (($(document).height()-topBar)*0.5-margin*2-padding*2-border*2)+'px');
	feedC.css('margin', margin+'px');
	feedC.css('padding', padding+'px');
	if (feed.items[current].imagem){
		feedC.append($('<img src="'+feed.items[current].imagem.url+'" />'));
	}
	var descC = $('<span>'+feed.items[current].descricao+'</span>');
	feedC.append(descC);
	var urlC = feed.items[current].url;
	feedC.click(function(){
		abirIFrame(urlC);
	});	
	
	current++;
	var feedD = $('<div class="feedD"></div>');
	feedD.append(feed.items[current].titulo);
	feedD.css('top', (($(document).height()-topBar)*0.5)+'px');
	feedD.css('left', ($(document).width()*0.75)+'px');
	var widthD = $(document).width()*0.25-margin*2-padding*2-border*2;
	feedD.css('width', widthD+'px');
	feedD.css('height', (($(document).height()-topBar)*0.5-margin*2-padding*2-border*2)+'px');
	feedD.css('margin', margin+'px');
	feedD.css('padding', padding+'px');
	if (feed.items[current].imagem){
		feedD.append($('<img src="'+feed.items[current].imagem.url+'" />'));
	}
	var descD = $('<span>'+feed.items[current].descricao+'</span>');
	feedD.append(descD);
	var urlD = feed.items[current].url;
	feedD.click(function(){
		abirIFrame(urlD);
	});	
	section.append(feedA);
	section.append(feedB);
	section.append(feedC);
	section.append(feedD);
	
	section.hide();
	$('body').append(section);
	
	
	
	// Page navigation
	var pagesNav = $('#pages');
	pagesNav.children().remove();
	var totalPages = (feed.items.length-feed.items.length%4)/4;
	for(var i=0;totalPages>1 && i<totalPages;i++){
		(function(curr){
			var nav = (i==page) ? $('<span class="goto_selected"></span>') : $('<span></span');
			nav.click(function (){
				verFeed(feedsIndex,curr-1);
			});
			nav.text(curr);
			pagesNav.append(nav);
		})(i+1);
	}
	

	var anotherVisible = $('.feed').length>1;
	if (anotherVisible){
		section.show();
		section.css('left', $(document).width()+'px');
		section.animate({
			left: 0
		}, tempoDeslocamento*1000, function(){
			removeOtherFeeds(section.attr('id'));
		});
	} else {
		section.fadeIn('normal', function(){
			removeOtherFeeds(section.attr('id'));
		});
	}
	$('#blocos').fadeOut('normal');
	$('#feedNav').fadeIn('normal');
}

function removeOtherFeeds(exceptId){
	console.log('remove all except: '+exceptId);
	$('.feed').each(function(index){
		if (this.id!=exceptId){
			console.log('removed:'+this.id);
			$(this).remove();
		}
	});
}

function abirIFrame(url){
	if (abrirNovaJanela){
		window.open(url, '_blank');
	} else {
		var section = $('#iframe');
	
		section.find('iframe:first').attr('src', url);
	
		section.fadeIn('normal');
		$("#backgroundPopup").fadeIn('normal');  
	}
}


// When document finished loading start js handlers
$(document).ready(function(){  
	$("#showAddRSSForm").click(function(){  
		centerAddRSSForm();  
		toggleAddRSSForm();  
	});
	
	$("#backgroundPopup").click(function(){  
		toggleAddRSSForm();  
	});  
	
	$(document).keydown(function(e){
		if(e.keyCode==27/*ESC*/ && $('#addRSSForm').is(':visible')){
			if ($('#iframe').is(':visible')){
				$('#iframe').fadeOut('normal', function(){
					$('#iframe').find('iframe:first').attr('src', 'about:blank');
				});
				$("#backgroundPopup").fadeOut('normal');  
			} else if(!$('#addRSSForm').is(':visible')){  
				toggleAddRSSForm();  
			}
		}  
	});
	
	$('#iframe').click(function(){
		$('#iframe').fadeOut('normal');
		$("#backgroundPopup").fadeOut("normal");  
	});
	
	$('#addrss').click(function(e){
		var val = $('input:radio[name=rssurl]:checked').val();
		//var val = $('#rssurl').val();
		if (val.length>0){
			adicionarRSS(val);
		}
	});
	
	$('#home').click(function(){
		removeOtherFeeds(null);
		$('#blocos').show();
		$('.feed').fadeOut('normal');
		$('#feedNav').fadeOut('normal');
	});
	
	// Cross domain AJAX GET
	/*$.ajax({
    	url: 'http://news.bbc.co.uk',
    	type: 'GET',
    	success: function(res) {
        	alert(res.responseText);
    	}
	});*/
	
	inicio();

	$(document).css('background-color', corFundo);
});